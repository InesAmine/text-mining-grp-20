
################################FONCTION FAITES MAISON##################################
# pour l'instant inexploitables

removeAccent <- function(dtm, nbMot){ 
  #cette fonction enlève les accents sur les voyelles (ça fonctionne pas snif)
  
  #dtm est un objet de type DocumentTermMatrix
  #nbMot est le nombre de mots différents présents dans la matrice. Il est visible dans l'environnement mais 
  #je ne savais pas comment le récupérer
  
  for (i in 1:nbMot){
    mot1 <- str_replace_all(dtm$dimnames$Terms[i],c('é','è','ê','ë'), 'e')[2] #la fonction donne un vecteur avec l'ancien mot et le nouveau mot
    mot2 <- str_replace_all(mot1,c('o','ö','ô'), 'o')[2]
    mot3 <- str_replace_all(mot2,c('à','â'), 'a')[2]
    mot4 <- str_replace_all(mot3,c('î','ï'), 'i')[2]
    mot5 <- str_replace_all(mot4,c('ù','û','ü'), 'u')[2]
    dtm$dimnames$Terms[i] <- mot5
  }
}

removeApostrophe <- function(dtm, nbMot){ 
  #cette fonction enlève les apostrophes intrusives 
  
  #dtm est un objet de type DocumentTermMatrix
  #nbMot est le nombre de mots différents présents dans la matrice. Il est visible dans l'environnement mais 
  #je ne savais pas comment le récupérer
  
  for (i in 1:nbMot){
    dtm$dimnames$Terms[i] <- str_replace(dtm$dimnames$Terms[i],"’","") 
  }
}

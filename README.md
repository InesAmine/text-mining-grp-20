Le code du projet reprend l'organisation générale annoncée dans le rapport. 

Trois dossiers concernent chacune des trois tables et contiennent donc le chemin classique (nettoyage, visualisation et catégorisation) qui a été suivi durant ce projet pour chaque table. 

Le dossier "Donnees" a été supprimé pour des raisons de confidentialité.
